﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using WindowsApp.Explorer.WPF.MVVM;
using WindowsApp.Explorer.WPF.Pages.FolderBrowse;
using WindowsApp.Explorer.WPF.Services;

namespace WindowsApp.Explorer.WPF.Pages
{
    public class MainViewModel : BindableBase
    {
        public Page CurrentPage { get; set; }

        public MainViewModel(PageService pageService)
        {
            pageService.PageChanged += (page) => CurrentPage = page;
            pageService.ChangePage(new FolderBrowsePage());
        }


        public ICommand Close => new DelegateCommand(() => App.Current.Shutdown());
        public ICommand Hide => new DelegateCommand(() => App.Current.MainWindow.WindowState = WindowState.Minimized);
        public ICommand Maximize => new DelegateCommand(() =>
        {
            var w = App.Current.MainWindow;

            w.WindowState = w.WindowState != WindowState.Maximized ? WindowState.Maximized : WindowState.Normal;
        });
    }
}
