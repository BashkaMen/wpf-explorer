﻿using System;
using System.Windows.Controls;

namespace WindowsApp.Explorer.WPF.Services
{
    public class PageService
    {
        public event Action<Page> PageChanged;


        public void ChangePage(Page page)
        {
            PageChanged?.Invoke(page);
        }
    }
}
