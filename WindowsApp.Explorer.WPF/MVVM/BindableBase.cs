﻿using System;
using System.ComponentModel;
using System.Reactive;
using System.Reactive.Linq;

namespace WindowsApp.Explorer.WPF.MVVM
{
    public class BindableBase : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;
        public IObservable<EventPattern<PropertyChangedEventArgs>> PropertyObservable { get; }

        public BindableBase()
        {
            PropertyObservable = Observable.FromEventPattern((EventHandler<PropertyChangedEventArgs> ev) => new PropertyChangedEventHandler(ev),
                 ev => PropertyChanged += ev,
                 ev => PropertyChanged -= ev);
        }

    }
}
