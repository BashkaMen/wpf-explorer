﻿using Serilog;
using Serilog.Events;
using SimpleInjector;
using WindowsApp.Explorer.WPF.Services;

namespace WindowsApp.Explorer.WPF
{
    public class Ioc
    {
        private readonly Container _container;

        public static Ioc Current { get; } = new Ioc();


        private Ioc()
        {
            _container = new Container();

            var logger = new LoggerConfiguration()
                .MinimumLevel.Verbose()
                .WriteTo.Debug()
                .WriteTo.File("Logs/log-.txt", LogEventLevel.Information, rollingInterval: RollingInterval.Day)
                .CreateLogger();

            _container.RegisterSingleton<ILogger>(() => logger);
            _container.RegisterSingleton<PageService>();

            _container.Verify();
        }

        public T Resolve<T>() where T : class => _container.GetInstance<T>();
    }
}
