﻿using WindowsApp.Explorer.WPF.Pages;

namespace WindowsApp.Explorer.WPF
{
    public class ViewModelLocator
    {
        public MainViewModel Main => Ioc.Current.Resolve<MainViewModel>();
    }
}
